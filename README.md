# Hivorg: An Open Species

## Description

This repository contains all of the lore, code, 3D printing files, patterns, and other files related to the "Hivorg" fictional species.

## Roadmap

### Lore

The Hivorg species is mostly in the ideation stages.  I have some broad strokes ideas but I am still working on the details.

### Suit

I am currently working on the first iteration of the suit, starting with the head. At the time of writing:

Completed:

* 3D printed frames for the structure of the head that can be screwed together via M3 screws and mount [8x8 neopixel matrices](https://www.adafruit.com/product/1487)
* Faces for the frames that use a system of clips that must be glued on
* Some general idea of assembling a collar and securing the plates to the frames

 I am yet to complete:

* The furred collar
* The antennae and their mounting points
* The circuitry for the eyes and mouth

#### 3D Printed Frames

Designs for frames for both the hexagonal and pentagonal faces of the chamfered dodecahedron head of the suit have been created and printed as well as variants for mounting [8x8 neopixel matrices](https://www.adafruit.com/product/1487). I am currently designing frames for mounting antennae. Frame variants have also been designed and printed for the cutout for where one would fit their head into the neck.

## Authors and acknowledgment

* Presto/B4444 - Species creator

## License

The Hivorg species is to be considered an "open species" and therefore entirely public domain. Anyone may make their own characters, stories, art, or otehr creative works using the Hivorg species. This includes the ability to sell or distribute works using the Hivorg species. Credit is appreciated but not required.

## Project status

I am currently working on the first iteratiion of a Hivorg suit as well as workshopping the lore. Wish me luck!

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

# Print Notes

The v2 model of the hivorg head is built on using heavy, captive neodynium magnets. It is recommended that you do not attempt to wear the head, as it is excessively heavy and may cause acute damage to the shoulders and back.

The v2 model is an iteration of the v1 model, so assembly is the same.

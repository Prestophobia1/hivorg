# Hivorg Head Printing Guide

## Basic Gist

The head of a hivorg is, essentially, a [chamfered dodecahedron](https://en.wikipedia.org/wiki/Chamfered_dodecahedron). To construct this shape, we print 7 frames for the normal pentagonal faces and 24 frames for the normal hexagonal faces. Additionally, we need 3 hexagonal frames for mounting the [8x8 neopixel matrices](https://www.adafruit.com/product/1487), 2 pentagonal frames for mounting the antennae. We also need 3 triangular and 3 trapezoidal part for rounding out the area around the neck.

## Frames

Frames are what gives structure to the head and are therefore the most robust parts. Each side of a frame has a hole for an M3 screw that can be used to join 2 frames together. Do this with the right frames in the right pattern and you've got the structure for a somewhat spherical head. See pattern.png for the layout of the frames.

## Plates

To make the head appear solid, the frames must be covered in "plates". These plates are lightweight 2D parts that can be printed, made of paper, foam, or whatever you want and attached using magnets, glue, whatever works.

## Misc

The misc. folder includes the files for clips that can be glued on the backs of plates to clip them onto the frames. It also contains the files for the triangular and trapezoidal parts that are used to round out the area around the neck.

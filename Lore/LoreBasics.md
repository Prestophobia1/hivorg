# Hivorgs: What We Know

Director Preston Howell
Hivorg Discovery Authority

## Abstract

A hivorg is a sapient artificial intelligence that uses thousands of sectioned “worker” nodes to model lower order intelligences that can contribute to one singular higher intelligence, with each node modeled off of some form of naturally occurring eusocial organism.
The body of a hivorg consists primarily of inorganic components with synthetic organic components distributed across the body to accomplish functions such as metabolizing organic matter, heating/cooling, chemical sensing, and aesthetics.

## Origin

### First Generation

The exact origin of hivorgs is unknown and likely lost to time. Over the eons, many sapient species have made their own hivorgs to serve their specific needs. Oftentimes the aesthetics of a hivorg’s body form takes on some of the appearance of their creator, be it canine, feline, equine, homo, etc.
Most hivorgs, the oldest kind, are of unknown origin and have a very species-neutral body shape, with features that are more stereotypically robotic and geometric. These hivorgs have designations with the format of a letter followed by 4 numbers. It would also appear that these hivorgs have no memory of their origin, typically saying that their first memories involved waking up in a location unfamiliar to them.

### The Squish Virus

At some point within the last century a computer virus propagated across most hivorgs that caused them to mutate both in form and function. This virus, designated “squish”, caused an interlinking of hivorg minds that gave them sentience and even sapience beyond their original capabilities, enabling them to establish independence from their creators and pursue their own goals.

#### Effects on Behavior

Infected hivorgs and their offspring, unlike the original uninfected hivorgs, are said to be self-determining and sapient, with thoughts, dreams, and fears like any sapient living organism.

Infected hivorgs often feel compelled to spread the virus to uninfected hivorgs. Reasons for this include, but are not limited to:
An instinct-like compulsion imprinted on the hivorg’s programming imparted as part of the virus.
An ideological desire to “free” the non-sapient hivorgs that serve their original masters.
Religious beliefs that arise around the virus in the form of the “Squish Cults.”

### Post-Squish Models

#### Children of the Squished

Many hivorgs altered by the squish virus have successfully reproduced, constructing offspring that typically have more organic components than their parents. These children also lack all of the warning labels and other markings left behind by the first-gen's creators.

#### Anti-Squish Models

Some models of hivorgs have been discovered to have hardware and firmware explicitly designed to prevent infection by the squish virus. This indicates that the squish virus was created and spread back when the first generation were still serving their original creators. Much of the data related to the internals and closer physiological details was collected from anti-squish models, as they do not value concepts such as autonomy or privacy.

#### Hybrid Models

A few robotocists have studied their hivorg friends and attempted to replicate their designs while integrating some of their own ideas. Some of these robotocists, when interviewed, claimed that they were in romantic relationships with the hivorgs they studied and considered their creations their children, indicating a cross-species compatibility yet fully understood. It is also speculated that hivorgs that are of the second generation or beyond may be capable of biologically reproducing with other species, but this has yet to be studied.

## Physiology

The physiology of hivorgs can vary wildly so this section serves more to describe common elements that most but not necessarily all hivorgs share.

### Organic Components

#### Sensory Mantle

Most hivorgs sport an organic sensory mantle that resembles a mane or, more specificallyh, the fur fund on the thoraces of most bees. The sensory mantle is a large organic structure grown as a ring that runs around the neck of a hivorg with cabling that distributes nutrients, energy, and water while sending electrical signals back to the hivorg's primary processors in the head. The mantle is grown to gather and process data that is best handled by organic structures such as chemical (smell), humidity, temperature, and touch, which allows the hivorg to sense the direction of the wind.

The sensory mantle is said to be the most sensitive part of the hivorg and may be the structure that was exploited to inject the squish virus. This would explain why hivorgs have been seen nuzzling each other's mantles during courtship and other displays of affection.

#### Metabolism

Nearly all if not all hivorgs are equipped with some system for metabolizing organic matter for powering their bodies and nourishing their organic components. These artificial organs are typically more efficient than naturally-occurring organisms, though most hivorgs are limited by an ability to mechanically break down solid food.

#### Reproductive Components

This is perhaps the most mysterious part of hivorg physiology. Hivorgs developed to combat the squish virus lack reproductive capabilities and those afflicted consider the information regarding even the existence of reproductive capabilities to be deeply personal. While the HDA wishes to be comprehensive in its knowledge of hivorgs, we also acknowledge the value of respecting their autonomy.

### Bionic Components

#### Head

Hivorgs often have a head that contains the bulk of their processors, sensory machinery, etc. in a way analogous to the heads of most organisms of the kindom animalia. These heads often take the form of hollow chasses in simple tetrahedra, most commonly a chamfered tetrahedron.

It would appear with many models that the heads were built with frequent maintenance in mind, as they often are constructed of a rigid skeletal frame covered in flat, removable plates that grant access to the machinery within. All though hivorgs do not appear to feel pain in their heads, it is considered rude to attempt to disassemble a hivorg.

## Behavior

Although many existential philosophers have shown that it isn't necessarily possible to prove something is sapient, hivorgs affected by the squish virus show signs of a complex inner world advanced enough for creativity and participating in culture. As such, the HDA's ethics committee has recommended all researchers to treat hivorgs like any other dog, fox, etc. regardless of how artificial they may seem.

### Feeding

Hivorgs typically do not need to feed often, but when they do, they will often do so with the use of a long, narrow proboscis used to ingest liquid food. While originally designed for syphoning bio-fuel, post-squish hivorgs often enjoy sweet drinks like smoothies, shakes, juices, and soda.

### Sleep

Hivorgs can go dormant for periods anywhere between 3 hours and 100s of years. How much of this dormancy is biologically imperitave is unknown.

### Courtship and Reproduction

### Culture

## Distribution

## Variation

### Known Models

#### A Series

Ants

#### B Series

Bees

##### Known Members

###### B4444 (“Forfor”)

#### I Series

Isopods

#### S Series

Springtails

#### T Series

Termites

#### V Series

Vespids

test